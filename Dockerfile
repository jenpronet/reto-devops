FROM ubuntu:latest

MAINTANER jenpronet@gmail.com

RUN apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install apt-utils \
    vim \
    htop;
RUN apt-get -y install dstat

RUN apt install npm

RUN git clone https://gitlab.com/jenpronet/reto-devops

RUN cd ./reto-devops

RUN npm install

RUN npm run test

RUN node index.js

RUN apt install jq

CMD ["/bin/echo", "Finalizaste la instalacion"]

RUN node index.js
